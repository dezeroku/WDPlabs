#include "agents.h"
#include <math.h>

double distance(struct agent a1, struct agent a2)
{	
	double distance_x ,distance_y, distance;

	distance_x=a1.x-a2.x;
	distance_y=a1.y-a2.y;
	
	distance=sqrt(pow(distance_x,2)+pow(distance_y,2));
	
	return distance;	
}